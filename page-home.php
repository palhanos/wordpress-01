<?php 
    // Template Name: Home Page
?>

<?php get_header(); ?>

<main>
    <section id="page-intro">
        <h2><?php the_field('titulo_landing_page') ?></h2>
        <h3><?php the_field('subtitulo_landing_page') ?></h3>
    </section>

    <section id="sobre">
        <h3><?php the_field('sobre_titulo') ?></h3>

        <div id="figures">
            <figure>
                <img src="<?php the_field('sobre_imagem_1') ?>" alt="café com creme formando desenho">
                <figcaption><?php the_field('sobre_legenda_1') ?></figcaption>
            </figure>

            <figure>
                <img src="<?php the_field('sobre_imagem_2') ?>" alt="botando creme no café">
                <figcaption><?php the_field('sobre_legenda_2') ?></figcaption>
            </figure>
        </div>

        <p>
            <?php the_field('sobre_descricao') ?>
        </p>
    </section>

    <section id="produtos">
        <div id="states">
            <div class="state">
                <div class="state-circle marrom"></div>
                <h4><?php the_field('produto_titulo_1') ?></h4>
                <p><?php the_field('produto_descricao_1') ?></p>
            </div>
            <div class="state">
                <div class="state-circle laranja"></div>
                <h4><?php the_field('produto_titulo_2') ?></h4>
                <p><?php the_field('produto_descricao_2') ?></p>
            </div>
            <div class="state">
                <div class="state-circle amarelo"></div>
                <h4><?php the_field('produto_titulo_3') ?></h4>
                <p><?php the_field('produto_descricao_3') ?></p>
            </div>
        </div>

        <button> saiba mais </button>
    </section>

    <section id="portfolio">
        <div class="filial">
            <img src="<?php the_field('portfolio_imagem_1') ?>" alt="filial botafogo">

            <div class="filial-text">
                <h4><?php the_field('portfolio_titulo_1') ?></h4>
                <p><?php the_field('portfolio_descricao_1') ?></p>
                <button>ver mapa</button>
            </div>
        </div>

        <div class="filial">
            <img src="<?php the_field('portfolio_imagem_1') ?>" alt="filial iguatemi">

            <div class="filial-text">
                <h4><?php the_field('portfolio_titulo_2') ?></h4>
                <p><?php the_field('portfolio_descricao_2') ?></p>
                <button>ver mapa</button>
            </div>
        </div>

        <div class="filial">
            <img src="<?php the_field('portfolio_imagem_1') ?>" alt="filial mineirão">

            <div class="filial-text">
                <h4><?php the_field('portfolio_titulo_3') ?></h4>
                <p><?php the_field('portfolio_descricao_3') ?></p>
                <button>ver mapa</button>
            </div>
        </div>
    </section>

    <section id="contato">
        <div id="contato-container">
            <div id="contato-text">
                <h2><?php the_field('contato_titulo') ?></h2>
                <h3><?php the_field('contato_subtitulo') ?></h3>
            </div>

            <form>
                <input type="email" placeholder="Digite seu e-mail">
                <input type="submit">
            </form>
        </div>
    </section>
</main>

<?php get_footer();?>