		<?php get_header(); ?>
		<main class="comeco">
		<h1>Cafés com a cara<br>do Brasil</h1>
		<p>Direto das fazendas de Minas Gerais</p>
	</main>
	
	<section class="sobre" id="sobre">
		<h2>Uma Mistura de</h2>
		<div class="container">
			<div class="sobre-item grid6">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-1.jpg">
				<h3>Amor</h3>
			</div>
			<div class="sobre-item grid6">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/cafe-2.jpg">
				<h3>Perfeição</h3>
			</div>
		</div>
		<p>O café é uma bebida produzida a partir dos grãos torrados do fruto do cafeeiro. É servido tradicionalm
aente quente, mas também pode ser consumido gelado. Ele é um estimulante, por possuir cafeína — geralmente 80 a 140 mg para cada 207 ml dependendo do método de preparação.</p>
	</section>
	
	<section class="produtos" id="produtos">
		<div class="container">
			<div class="produtos-item grid4">
				<h2 class="produtos-paulista">Paulista</h2>
				<p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
			</div>
			<div class="produtos-item grid4">
				<h2 class="produtos-carioca">Carioca</h2>
				<p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
			</div>
			<div class="produtos-item grid4">
				<h2 class="produtos-mineiro">Mineiro</h2>
				<p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo noutras regiões: o João Alberto Castelo Branco trouxe mudas do Pará</p>
			</div>
		</div>
		<a class="produtos-bt" href="https://www.youtube.com/watch?v=B5QxaMB9r-4">Saiba Mais</a>
	</section>
	
	<section class="unidades" id="unidades">
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/botafogo.jpg" alt="Brafé  Botafogo">
			</div>
			<div class="grid6">
				<h2>Botafogo</h2>
				<p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
				<a>Ver Mapa</a>
			</div>
		</div>
		
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/iguatemi.jpg" alt="Brafé  Iguatemi">
			</div>
			<div class="grid6">
				<h2>Iguatemi</h2>
				<p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
				<a>Ver Mapa</a>
			</div>
		</div>
		
		<div class="unidades-item container">
			<div class="grid6">
				<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mineirao.jpg" alt="Brafé  Mineirão">
			</div>
			<div class="grid6">
				<h2>Mineirão</h2>
				<p>As condições climáticas não eram as melhores nessa primeira escolha e, entre 1800 e 1850, tentou-se o cultivo.</p>
				<a>Ver Mapa</a>
			</div>
		</div>
	
	</section>
	
	<section class="contato" id="contato">
		<div class="container">
			<div class="contato-info grid6">
				<h2>Assine Nossa Newsletter</h2>
				<p>promoções e eventos mensais</p>
			</div>
			<form class="grid6">
				<label>E-mail</label>
				<input type="text" placeholder="Digite seu e-mail">
				<button type="submit">Enviar</button>
			</form>
		</div>
	</section>
	<?php get_footer(); ?>
	



